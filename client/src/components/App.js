import React, {Component} from 'react';
import { connect } from 'react-redux';
import { BrowserRouter, Route } from 'react-router-dom';
import * as actions from '../actions';

import Header from './header/Header';
import Landing from './Landing';
import Dashboard from './Dashboard';
import Profile from './Profile';

class App extends Component {
  componentDidMount() {
    this.props.fetchUserFromInstagram();
    this.props.fetchUserFromDb();
  }
  
  render() {
    return (
        <BrowserRouter>
          <div>
          <Header/>
          <Route exact path="/" component={Landing}/>
          <Route path="/user/dashboard" component={Dashboard}/>
          <Route path="/user/profile" component={Profile}/>
          </div>
        </BrowserRouter>
    );
  }
}

export default connect(null, actions)(App);