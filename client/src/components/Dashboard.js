import React, { Component } from 'react';
import { connect } from 'react-redux';

class Dashboard extends Component {
    
    renderContent(){
        console.log(this.props.auth);
    }

    render(){
        return(
           <div>
              Dashboard
              {this.renderContent()}
           </div>
        );
    }
    
}

function mapStateToProps({ auth }){
    return { auth };
}

export default connect(mapStateToProps)(Dashboard);