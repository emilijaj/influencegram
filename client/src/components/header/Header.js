import React, { Component } from "react";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import { Link } from "react-router-dom";
import { AppBar, Toolbar } from "@material-ui/core";
import styled from "styled-components";
import RaisedButton from "../buttons/RaisedButton";
import HeaderLoggedIn from "./HeaderLoggedIn";
import PropTypes from "prop-types";

const TitleLogo = styled.div`
  font-family: "Oswald", sans-serif;
  text-align: left;
  color: rgba(0, 0, 0, 0.87);
  font-size: 25px;

  @media (max-width: 767px) {
    font-size: 20px;
  }
`;

const styles = {
  flex: {
    flex: 1,
    color: "black"
  }
};

class Header extends Component {
  renderContent() {
    switch (this.props.auth) {
      case null:
        return;
      case false:
        return (
          <a href="/auth/instagram">
            <RaisedButton text="Login" />
          </a>
        );
      default:
        return (
          <HeaderLoggedIn
            instagramPhoto={
              this.props.auth ? this.props.auth.instagramProfilePicture : ""
            }
          />
        );
    }
  }

  render() {
    const { classes } = this.props;
    return (
      <AppBar color="default" position="static">
        <Toolbar>
          <Link
            className={classes.flex}
            to={this.props.auth ? "/user/dashboard" : "/"}
          >
            <TitleLogo>Peek your Followers</TitleLogo>
          </Link>
          {this.renderContent()}
        </Toolbar>
      </AppBar>
    );
  }
}

function mapStateToProps({ auth }) {
  return { auth };
}

Header.propTypes = {
  classes: PropTypes.object.isRequired
};

export default connect(mapStateToProps)(withStyles(styles)(Header));
