import React, { Component } from 'react';
import { connect } from 'react-redux';


class Profile extends Component {

    renderContent(){
        return (
            <div>{this.props.user.instagramFollowedBy > 100 ? 'Influencer' : 'Try harder!'}</div>
        );
    }

    render(){
        return(
           <div>
               {this.renderContent()}
           </div>
        );
    }
    
}

function mapStateToProps({ user }){
    return { user };
}

export default connect(mapStateToProps)(Profile);