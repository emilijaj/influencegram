import axios from 'axios';
import { FETCH_USER_DB, FETCH_USER_INSTAGRAM } from './types';


export const fetchUserFromDb = () => async dispatch => {
    const res = await axios.get('/user/profile');
    dispatch({
        type: FETCH_USER_DB,
        payload: res.data
    });
}


export const fetchUserFromInstagram = () => async dispatch => {
    const res = await axios.get('/user/current_user');
    dispatch({
        type: FETCH_USER_INSTAGRAM,
        payload: res.data
    });
}

