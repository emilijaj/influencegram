const instagramApi = require('instagram-node').instagram();
const requireLogin = require('../middlewares/requireLogin');
const mongoose = require('mongoose');
const User = mongoose.model('users');

module.exports = app => {

    app.get(
        '/user/current_user',
        async (req, res) => {
            res.send(req.user);
        }
    );

    app.get(
        '/user/dashboard',
        (req, res) => {
            instagramApi.use({
                access_token: req.user.accessToken
            });

            instagramApi.user_media_recent(req.user.instagramId, {}, function (error, result, pagination, remaining, limit) {
                if (error) res.json(error);

                res.send(result);
            })
        }
    );

    app.get(
        '/user/profile',
        requireLogin,
        async (req, res) => {
            const userData = await User.findById({
                _id: req.user._id
            });
            res.send(userData);
        }
    );
};